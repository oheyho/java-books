# 为何整理这份电子书？

猿人从事Java开发多年，从最初的小白，一步步通过自己不断的学习、摸索，终于走上了码农这条没有回头的路。学习是无止境的，有时零零散散的学习资料难以将我们的知识成体系的串联起来。为此，为了让大家在学习Java的道路上成系统的学习，少走弯路，猿人为大家整理了1000+本Java开发精华电子书，毕竟现在电子书也是挺贵的，哈哈，希望能给大家带来帮助。

>顺带宣传一下我关注的这个原创公众号：专注于 Java 编程技术和程序员软实力的方方面面，欢迎小伙伴们扫一扫关注一下，一定会大有所获。

![猿码天地](https://ymtd-1307390667.cos.ap-guangzhou.myqcloud.com/img-wechat/%E5%BE%AE%E4%BF%A1%E5%85%AC%E4%BC%97%E5%8F%B7.jpg)  

# 电子书目录

- [01、Java入门](#Java入门)
- [02、并发编程](#并发编程)
- [03、底层](#底层)
- [04、常用框架](#常用框架)
- [05、性能优化](#性能优化)
- [06、设计模式](#设计模式)
- [07、工具](#工具)
- [08、计算机网络](#计算机网络)
- [09、操作系统](#操作系统)
- [10、数据库](#数据库)
- [11、数据结构与算法](#数据结构与算法)
- [12、大数据](#大数据)
- [13、架构设计](#架构设计)
- [14、Linux知识](#Linux知识)
- [15、面试](#面试)
- [16、扩展](#扩展)
- [17、管理](#管理)
- [18、加餐](#加餐)
- [19、容器化技术](#容器化技术)
- [20、云计算](#云计算)
- [21、微服务](#微服务)
- [22、活着](#活着)
- [23、领域设计](#领域设计)
- [00、免责声明](#免责声明)

# 阅读建议  

对于刚入行的码农或者刚毕业的大学生，想应聘【初级 Java 工程师】，那只需要阅读Java入门、工具、常用框架和数据库方面的书籍就行了。

对于工作几年，如果想应聘【Java 高级工程师】，那么就需要阅读并发编程、底层、性能优化方面的书籍。

如果你是一个有追求的人，不甘心只做一个底层码农，那么就要着手阅读设计模式、操作系统、计算机网络、数据结构与算法等方面的书籍。

记住一点，在应聘之前，请恶补一下面试方面的资料，毕竟面试考的知识面要更全，就像闯关游戏，非常具有挑战性。

如果时间充沛，大数据、架构设计、管理方面的书籍可以读起来。

如果还有时间，容器化技术、云计算、微服务、扩展、加餐、领域设计等方面的书籍，就可以读起来了。

技术学累了，可以读一读理财金融方面的书籍，比如说香帅北大金融学课、李笑来的学习学习再学习，管理方面的，高效能人士的七个习惯等等。

最后，不管怎样，活着最重要！

# Java入门

- GitHub 上标星 115k+ 的 Java 教程 [百度云下载链接](https://pan.baidu.com/s/11X8uX6LYcL7jZf3eGZqfSQ) 密码：e5ja
- Head First Java 中文高清版 [百度云下载链接](https://pan.baidu.com/s/1xXmUUm2E2kapWcpDNNbjjg) 密码：h4q8
- Java 8 实战 [百度云下载链接](https://pan.baidu.com/s/1ADwo2SF3we9QOYSsUj_zOg) 密码：iz3j
- Java 基础核心总结 [百度云下载链接](https://pan.baidu.com/s/1xdWVCtDm0sQQd2LAvnTT-g) 密码：qu8w
- JAVA核心知识点整理 [百度云下载链接](https://pan.baidu.com/s/1tH8MROb3g9_Oo8oEJjCurQ) 密码：gs6k
- 疯狂JAVA讲义（第2版）--带书签版 [百度云下载链接](https://pan.baidu.com/s/12BR9ERnyIYgy2HiLkGzaHQ) 密码：u35z
- 零基础学 Java [百度云下载链接](https://pan.baidu.com/s/13In364XBG8hase4GHUlJqA) 密码：iga7

# 并发编程

- 深入浅出 Java 多线程 [百度云下载链接](https://pan.baidu.com/s/1Hr_KJgYjIvJAbm0OXGJ97g) 密码：hdfd
- JAVA并发编程实战 [百度云下载链接](https://pan.baidu.com/s/1x-Ih94IGOG4MYHYmlc7s1w) 密码：pl8l
- 深入理解java内存模型 [百度云下载链接](https://pan.baidu.com/s/153AJKVbZE_b-nPJLNdWLfQ) 密码：3fv8
- 线程池预习资料 [百度云下载链接](https://pan.baidu.com/s/1Cl1rL3rGrGyDy4nBfHzAEA) 密码：v85l
- 最牛并发编程总结 [百度云下载链接](https://pan.baidu.com/s/10Bb1lpLBioG1irtGq4Kgyw) 密码：qa93
- CPU内存一致性模型和指令重排序-论文 [百度云下载链接](https://pan.baidu.com/s/1tILdUw71pW91McOIYa72TA) 密码：xfa2

# 底层

- Java JDK 7学习笔记 [百度云下载链接](https://pan.baidu.com/s/1ayXDJdLgxU9yQ1w_DHb2Vg) 密码：3q1z
- 深入理解 Java 虚拟机总结 [百度云下载链接](https://pan.baidu.com/s/1sDeJ8DsWThDVPRbAoid3vA) 密码：frwo
- 深入理解Java内存模型 [百度云下载链接](https://pan.baidu.com/s/1J7OqGbcoELFU9ukPEDBO8g) 密码：y9r6
- 深入理解Java虚拟机（第二版-带目录） [百度云下载链接](https://pan.baidu.com/s/1dUxzS5OqlSPj-coDyGExNA) 密码：4yk5

# 常用框架

- MyBatis 入门指南松哥版 [百度云下载链接](https://pan.baidu.com/s/1XUv1_wdOLChsntCTc4o_1w) 密码：t7fd
- Netty进阶之路 跟着案例学Netty_完整版 [百度云下载链接](https://pan.baidu.com/s/1pY4ODZtGWgE0FGkRnoyptw) 密码：70t5
- Netty权威指南 第2版_完整版 [百度云下载链接](https://pan.baidu.com/s/1lfxgkGxfbl_v9Rkinu4TzA) 密码：9zrw
- Netty实战中文高清版 [百度云下载链接](https://pan.baidu.com/s/1id8BXumjEOD3sNPN6R5wrQ) 密码：u4u7
- Spring Bean 详解 [百度云下载链接](https://pan.baidu.com/s/1Zr6BUX97SitgmaqxRyKxXg) 密码：6nie
- Spring 入门指南松哥版 [百度云下载链接](https://pan.baidu.com/s/1jOno0cM5TsSrtUYgTXvqhA) 密码：1j2v
- spring2.0技术手册 [百度云下载链接](https://pan.baidu.com/s/1aP8hNq2AJWbUCZjWM5zReA) 密码：yn0k
- SpringMVC 入门指南松哥版 [百度云下载链接](https://pan.baidu.com/s/1EFcFMLIEFWF2gm09ARJmAA) 密码：cey5

# 性能优化

- Effective Java 3rd edition [百度云下载链接](https://pan.baidu.com/s/1pbfZr6tDjcMmdoCXvKDjKw) 密码：gak9
- Java程序性能优化  让你的Java程序更快、更稳定 [百度云下载链接](https://pan.baidu.com/s/1v-39CZMKsuJqc_kK0bxO5Q) 密码：7en2
- JVM性能优化相关问题 [百度云下载链接](https://pan.baidu.com/s/1xp2hZvPbc6vGgpXb21xZHA) 密码：n7ij
- MySQL性能优化的21个最佳实践 [百度云下载链接](https://pan.baidu.com/s/1qKecZ3SDK089LybqGvifVw) 密码：amcr
- Tomcat优化相关问题 [百度云下载链接](https://pan.baidu.com/s/11cQuDdbBC4ucasD1eAJJZg) 密码：qwp2
- 代码整洁之道 [百度云下载链接](https://pan.baidu.com/s/16wM6_m84z6uMXEFbRslAcg) 密码：kvj1
- 码出高效：Java开发手册 [百度云下载链接](https://pan.baidu.com/s/1NVnWMOqn6wsGLOs4q_JoNg) 密码：1hqp
- 重构_改善既有代码的设计 [百度云下载链接](https://pan.baidu.com/s/1CzE5ho6HyCjqKQKDQkayuw) 密码：z0sy

# 设计模式

- 23种设计模式知识要点 [百度云下载链接](https://pan.baidu.com/s/1V2TFNzC80jJARUZCHXFD7g) 密码：iyi7

# 工具

- GitHub入门与实践完整版 [百度云下载链接](https://pan.baidu.com/s/1PkmgQzWzHFl9iMlEMwjCSg) 密码：uyly
- Git权威指南-目录完美-完整版 [百度云下载链接](https://pan.baidu.com/s/1nlVUR-CL7eUHCBJVig39Hw) 密码：pott
- Maven 入门指南-松哥版 [百度云下载链接](https://pan.baidu.com/s/1sapaLNUjBC-CEysury77HQ) 密码：1ghj
- 日志系统手册（Log4j、SLF4J、Logback、Log4j 2） [百度云下载链接](https://pan.baidu.com/s/1ZQ_k_RWXe4evBdT0k8GAEQ) 密码：395v
- 嵩山版 Java 开发手册 [百度云下载链接](https://pan.baidu.com/s/16dMTnhRKyw6Ixr3_hazknQ) 密码：m1bs

# 计算机网络

- http超全混总 [百度云下载链接](https://pan.baidu.com/s/1J2KKRn4_QK33QAA45D6CqA) 密码：ftxm
- TCP IP网络编程(韩)尹圣雨 [百度云下载链接](https://pan.baidu.com/s/1UOFly3T5OxlkzveiWsMsOQ) 密码：ozq4
- TCP-IP 详解 [百度云下载链接](https://pan.baidu.com/s/1iDhyKjupPm3Tb6AUe0ckFw) 密码：l74c
- 计算机网络-自顶向下方法 [百度云下载链接](https://pan.baidu.com/s/1fK6Ywj-G64xUl4NyJ3gjSA) 密码：5ctc
- 图解HTTP【上野宣】 [百度云下载链接](https://pan.baidu.com/s/1ZnHI3TTpVAfAdTNmaKh01g) 密码：uh7s
- 图解tcpip(第5版) [百度云下载链接](https://pan.baidu.com/s/1ShMTadQ7l7sJCFNKyX-0sA) 密码：ly0d
- 网络是怎样连接的 [百度云下载链接](https://pan.baidu.com/s/1LUETy4z0VdiFcdi601-1Iw) 密码：jr6f

# 操作系统

- 程序是怎样跑起来的 [百度云下载链接](https://pan.baidu.com/s/1HwhPIfm7xLHtTxEwx0WlSQ) 密码：v64x
- 计算机是怎样跑起来的 [百度云下载链接](https://pan.baidu.com/s/1E4nRmlBb5kCSavqOS6mPTw) 密码：hcag
- 认识操作系统 [百度云下载链接](https://pan.baidu.com/s/1x_EBQPl0DFGHYflM0yy0BQ) 密码：zbjl
- 深入理解计算机系统 [百度云下载链接](https://pan.baidu.com/s/1kPLmBFuOoubT_2elkoizvg) 密码：86of

# 数据库

- MySQL必知必会（高清版） [百度云下载链接](https://pan.baidu.com/s/1b-51P6ZPXo2L8eXs8lwO6Q) 密码：qdnw
- MySql优化 [百度云下载链接](https://pan.baidu.com/s/1BDnRmnE5cCou4UZ3EgRLJg) 密码：fcbp
- SQL+Server+2008实战 [百度云下载链接](https://pan.baidu.com/s/19eBAO9CRrGjeTPGvS1rl1Q) 密码：58zg
- SQL必知必会 [百度云下载链接](https://pan.baidu.com/s/1YFFLQSWStVfLJQwY_x7F2g) 密码：ris1
- 高性能MySQL-第3版 [百度云下载链接](https://pan.baidu.com/s/102la-q7e6JWKA2fbn2r0xA) 密码：i43q
- 深入浅出MySQL全文第二版 [百度云下载链接](https://pan.baidu.com/s/1K5Rt2UcmWaO7pGf3Ns_GZg) 密码：ezki
- 数据库系统基础教程原书第3版 [百度云下载链接](https://pan.baidu.com/s/152UHB2swFasdfVjESuVqwQ) 密码：y0rx
- 自己动手设计数据库 [百度云下载链接](https://pan.baidu.com/s/1R30EWtF9Hcga0hbTfystfw) 密码：bj82

# 数据结构与算法

- BAT LeetCode 刷题手册 [百度云下载链接](https://pan.baidu.com/s/1icvSCuTTwmjX6wmWjZXgwA) 密码：ctif
- JAVA经典算法 [百度云下载链接](https://pan.baidu.com/s/1RWBHbILp-wA9YHpdHpNC4Q) 密码：ah2g
- 啊哈算法 [百度云下载链接](https://pan.baidu.com/s/18CZobTTYEsdB-KdVeLcuig) 密码：keb8
- 编程珠玑 [百度云下载链接](https://pan.baidu.com/s/10MVcH19R-z0AgWEylQivPA) 密码：w07m
- 大话数据结构 [百度云下载链接](https://pan.baidu.com/s/1j2tmV-wBvZ5nNjCX9yoe_g) 密码：e7k7
- 数据结构与算法分析 Java 描述 [百度云下载链接](https://pan.baidu.com/s/1Cta911N5lKHKdq_VmzNnww) 密码：zls0
- 算法（第四版） [百度云下载链接](https://pan.baidu.com/s/1n_JwVkGnANeyZp1XQW16Hg) 密码：7f3t
- 算法导论中文第三版 [百度云下载链接](https://pan.baidu.com/s/1kd631-rjrAbXZ6x7GLooxg) 密码：f8xc
- 算法图解 [百度云下载链接](https://pan.baidu.com/s/12lkYZTVm6LbctHHurYMvag) 密码：0zwn

# 大数据

- Hadoop技术内幕 深入理解MapReduce架构设计与实现原理 [百度云下载链接](https://pan.baidu.com/s/16vgrPPogpnvO7kTxsEHViA) 密码：n0re
- Hadoop权威指南 [百度云下载链接](https://pan.baidu.com/s/1skPajSPB3TpUOCM5JbE80Q) 密码：l1g1
- Kafka权威指南 [百度云下载链接](https://pan.baidu.com/s/17N8afYM8S2arxoJqgzyPTA) 密码：iw4g
- Spark快速大数据分析   [百度云下载链接](https://pan.baidu.com/s/1R_GAl3AUQHtxcbJOXGOZ2g) 密码：l66z
- Spark快速数据处理完整版 [百度云下载链接](https://pan.baidu.com/s/1gjV_qSb8IQUIAlF9_MLHxQ) 密码：up53
- 大数据-涂子沛 [百度云下载链接](https://pan.baidu.com/s/1Lni2gUvi3N0fgN7cM7Jwzg) 密码：ovs4
- 数据之巅-涂子沛 [百度云下载链接](https://pan.baidu.com/s/1XbkLsHPMazDJkc6T_ReD7w) 密码：1mcb

# 架构设计

- 大型网站技术架构 核心原理与案例分析-李智慧 [百度云下载链接](https://pan.baidu.com/s/1wHS0J6LpLIIW8aPl9Y-TSw) 密码：jklv
- 大型网站系统与Java中间件实践 [百度云下载链接](https://pan.baidu.com/s/1oH3D8VbK9sLhwJNxX7Ig-A) 密码：fxzd
- 高性能高并发服务器架构 [百度云下载链接](https://pan.baidu.com/s/1CSr0yqbDvMoSj35qphLoZQ) 密码：10fq
- 架构风格与基于网络的软件架构设计-博士论文 [百度云下载链接](https://pan.baidu.com/s/1ypHv-fbQSYIOhL9hhxPD5Q) 密码：l1fi
- 架构之美(清晰中文完整版) [百度云下载链接](https://pan.baidu.com/s/12cyZzqTLN_GwPwq93FQbjA) 密码：ddn5
- 亿级流量网站架构核心技术 [百度云下载链接](https://pan.baidu.com/s/1ZYShPYeMG2Q9XKbynX_aZw) 密码：zm0y

# Linux知识

- 鸟哥的Linux私房菜 [百度云下载链接](https://pan.baidu.com/s/16qasasfFqZ1nLXfFkFCVUQ) 密码：42vp
- 深入理解LINUX网络技术内幕 [百度云下载链接](https://pan.baidu.com/s/1o8pwrb-fMf8kf0QadsphDQ) 密码：p3e6
- 循序渐进Linux第2版 [百度云下载链接](https://pan.baidu.com/s/18yiE2EM_wqZ5cWsu7YhZrA) 密码：mj2n
- Linux-UNIX系统编程手册（上、下册） [百度云下载链接](https://pan.baidu.com/s/1-GvnqzLn5p1vPce3Z_SflQ) 密码：v4yg
- Linux高性能服务器编程 [百度云下载链接](https://pan.baidu.com/s/1IjZ-RG8qCmD3ahE3yLQyHw) 密码：lnae
- UNIX 环境高级编程版 [百度云下载链接](https://pan.baidu.com/s/1wI4Yp56qgf92p-Cl2w_hOg) 密码：6yu2
- linux内核源代码情景分析 [百度云下载链接](https://pan.baidu.com/s/1aToo2JHO2Me2rRR7fExR0g) 密码：m788
- 深入Linux内核架构 [百度云下载链接](https://pan.baidu.com/s/1WBHGQsp3bRwkqeGO4V134A) 密码：vkk3
- 深入理解linux内核中文第三版 [百度云下载链接](https://pan.baidu.com/s/1epQkjP36vyyva8RJIjhA_A) 密码：fg3y

# 面试

- 2020年字节跳动Java 工程师面试题 [百度云下载链接](https://pan.baidu.com/s/16mKR0pH9heSVKbuPjNRQIw) 密码：epbq
- 2020最新Java面试题资料 [百度云下载链接](https://pan.baidu.com/s/12u6iJEPM7-i8djuu9hIW8w) 密码：ph6u
- Dubbo 面试题 [百度云下载链接](https://pan.baidu.com/s/1n5fiYxcaM4g0W62HLSvqAA) 密码：hps5
- JavaGuide 面试突击最新版 [百度云下载链接](https://pan.baidu.com/s/13LVBy5xujE88zN_cqlAIkw) 密码：bo4c
- 一线互联网企业面试题 [百度云下载链接](https://pan.baidu.com/s/1YZNwAPuzBTwCjcPWZLEKdg) 密码：vp6p
- Java常见面试题 [百度云下载链接](https://pan.baidu.com/s/19iT8CslZg-cWgq-6ioAvEg) 密码：6rzi
- JAVA核心面试知识整理 [百度云下载链接](https://pan.baidu.com/s/1GkkfVwt9vfvhQOMpZuagHQ) 密码：3wkx
- Java架构面试专题（含答案）和学习笔记 [百度云下载链接](https://pan.baidu.com/s/1hBrG90nmIUn6NQXC4yNwZA) 密码：l3h6
- Java面试题以及答案 [百度云下载链接](https://pan.baidu.com/s/1YRuETwAnHdmUV8DKYypCNQ) 密码：kcwb
- Java面试资料合集 [百度云下载链接](https://pan.baidu.com/s/1N4HU2K8W1gDdxFwAmZCDjw) 密码：jzg1
- Spring 面试题 [百度云下载链接](https://pan.baidu.com/s/1U0nv7v3evAqt7HovHCgiSQ) 密码：6fuv
- 程序员面试宝典 [百度云下载链接](https://pan.baidu.com/s/1udR66jpyYmykgUunJAOjJw) 密码：e3g2
- 剑指Offer：名企面试官精讲典型编程题（第2版） [百度云下载链接](https://pan.baidu.com/s/1vokyUBy7L0N_pgRqu5VocQ) 密码：460l
- 面试必问之jvm与性能优化 [百度云下载链接](https://pan.baidu.com/s/1rHP8nlPQI_vrnto0hWQ84Q) 密码：li4c
- v4.0-JavaGuide面试突击版 [百度云下载链接](https://pan.baidu.com/s/1uY94_1IKzp2ZnINHVyUV4w) 密码：24np

# 扩展

- CDN技术 [百度云下载链接](https://pan.baidu.com/s/1orq4m5q-42Se51qhi_EUfQ) 密码：48n1
- Python [百度云下载链接](https://pan.baidu.com/s/1Q9JasLJmFcDhiaT2xnBcnw) 密码：yvby
- 机器学习与实战 [百度云下载链接](https://pan.baidu.com/s/1WHK0eUslf6M_WJW5zGU_mg) 密码：8e06
- 领域驱动设计.软件核心复杂性应对之道 [百度云下载链接](https://pan.baidu.com/s/1oZmjw-gVlGEnwDcdthsSBQ) 密码：myl6
- 领域驱动设计精简版 [百度云下载链接](https://pan.baidu.com/s/1vuJez6RctL3H66o_OmmSkw) 密码：z0xd
- 有效的单元测试 [百度云下载链接](https://pan.baidu.com/s/1dJkO3CKzXLdgPO4-PHQE5Q) 密码：hj2o

# 管理

- 大教堂与集市（中文版首次出版） [百度云下载链接](https://pan.baidu.com/s/13vFrjqs5mqrMZT0Gzkx5yQ) 密码：zx9b
- 人件 原书第3版 [百度云下载链接](https://pan.baidu.com/s/1T0NAQcz82NaX2lK9EYd04A) 密码：e33q
- 人月神话 [百度云下载链接](https://pan.baidu.com/s/1TUhEpa28z6eLnguTdoUIEQ) 密码：5swk
- 深入浅出项目管理 [百度云下载链接](https://pan.baidu.com/s/1D4RgXjDc3WH1fVHnJp3N4A) 密码：kx1e

# 加餐

- how-to-be-a-programmer-cn [百度云下载链接](https://pan.baidu.com/s/1IfyByKdfV9YLOaHPXo2lOg) 密码：dkhq
- 阿里技术参考图册（算法篇） [百度云下载链接](https://pan.baidu.com/s/1q4VXw24Nah86jAIWO7lCTQ) 密码：ehbb
- 阿里技术参考图册（研发篇） [百度云下载链接](https://pan.baidu.com/s/1HUrWqEbFLzrgynwLEhsFhQ) 密码：mfc6
- 编程人生 [百度云下载链接](https://pan.baidu.com/s/1db85zHBGnBuwtTK9iyAkJg) 密码：klwx
- 编程之美-高清-带目录版 [百度云下载链接](https://pan.baidu.com/s/1955geEJG-HZD5ZvQj2nEWA) 密码：9z2j
- 产品经理必读书籍盘点推荐 [百度云下载链接](https://pan.baidu.com/s/1VPsYKY2R95pyGNWYpr15MA) 密码：4vos
- 程序员必知的硬核知识大全 [百度云下载链接](https://pan.baidu.com/s/1Ctw5OFSTOY-bEy_8ndT52g) 密码：snvh
- 程序员的职业素养 [百度云下载链接](https://pan.baidu.com/s/1Q8UYNagOEHzAvBs0moryRQ) 密码：q2m8
- 程序员内功修炼-V2.0 [百度云下载链接](https://pan.baidu.com/s/1BOACpH8TlmB-VUkBcl9tSw) 密码：vkry
- 程序员修炼之道：从小工到专家 [百度云下载链接](https://pan.baidu.com/s/1WRIbxQUUWZ_vkQI7Q9hXvQ) 密码：3g6y
- 代码大全 [百度云下载链接](https://pan.baidu.com/s/1S0XrmuxKMq5md1v45foP9g) 密码：apfp
- 黑客与画家 [百度云下载链接](https://pan.baidu.com/s/1vATK0jiGLJJIys9S6ku9EQ) 密码：rffb
- 奇思妙想：15 位计算机天才及其重大发现 [百度云下载链接](https://pan.baidu.com/s/1yIuMUtIp-7t3olDF90uQGA) 密码：nlvm
- 如何变得有思想  阮一峰博客文集 [百度云下载链接](https://pan.baidu.com/s/129fOKtqIffB6L8Vrlkbkjg) 密码：gr87
- 设计原本（中文版） [百度云下载链接](https://pan.baidu.com/s/17sFjfN_ynfzcMHiNLEOZRQ) 密码：vku8
- 数学之美 [百度云下载链接](https://pan.baidu.com/s/1TBGVhdAa4tz_W213uviOgA) 密码：6v68
- 淘宝技术这十年 [百度云下载链接](https://pan.baidu.com/s/1AXe1mZ5Zg0eyWnU11VrExg) 密码：bdu5
- 图灵的秘密 [百度云下载链接](https://pan.baidu.com/s/1Y6tV-trV5yZGVFCh13Tk_g) 密码：9nea
- 我编程我快乐 [百度云下载链接](https://pan.baidu.com/s/1AV_MB49mKFHItLe-1buwIg) 密码：02hz
- 香帅北大金融学课 线下大课 [百度云下载链接](https://pan.baidu.com/s/1hSaVox8r-AXX95givzkLwQ) 密码：ut2v
- 学习学习再学习 [百度云下载链接](https://pan.baidu.com/s/176YHkpRUOjvaZjyFtN9ZuQ) 密码：6nym
- 卓有成效的程序员 [百度云下载链接](https://pan.baidu.com/s/1DbS0URSKkb6VURZHDzSgRA) 密码：qgrg

# 容器化技术

- Docker入门指南松哥版 [百度云下载链接](https://pan.baidu.com/s/15Z0mgG3qsmTrlfadWlYcIg) 密码：ieq4
- 第一本Docker书 [百度云下载链接](https://pan.baidu.com/s/17GewshfBuACQL0RXkUhouw) 密码：resm
- 每天5分钟玩转Docker容器技术 [百度云下载链接](https://pan.baidu.com/s/1S8FyspE6ayk0ZPVdM58row) 密码：93w1
- 每天5分钟玩转Kubernetes [百度云下载链接](https://pan.baidu.com/s/1tV_prLNftfPzDq7aNyqrDw) 密码：r8k1

# 云计算

- 大话云计算 [百度云下载链接](https://pan.baidu.com/s/1Jv7lyo9d3AhBTizKFPo4QQ) 密码：e9jc

# 微服务

- Spring Boot 技术栈快速实战课程 [百度云下载链接](https://pan.baidu.com/s/1Byldv6XhE3z-tQ8-QQFLzA) 密码：kpk5
- 深入理解Spring Cloud与微服务构建 [百度云下载链接](https://pan.baidu.com/s/1IpgjKQWcNhp8TjPrH9o3qw) 密码：eaz0
- 微服务：从设计到部署 [百度云下载链接](https://pan.baidu.com/s/1SbDugUmYAvATzyIIRZVBHA) 密码：nm8r
- SpringBoot&SpringCloud学习源码和文档 [百度云下载链接](https://pan.baidu.com/s/1Um34vEPEfBEPxU4b-hhCOw) 密码：8noi
- SpringCloudAlibaba技术文档 [百度云下载链接](https://pan.baidu.com/s/1IEymaQPGOwkZAv6kL0wV3A) 密码：62ce

# 活着

- 程序员健康指南 [百度云下载链接](https://pan.baidu.com/s/1mixCPux83LflLMcPjqTVJw) 密码：sxt1
- 颈椎康复指南 [百度云下载链接](https://pan.baidu.com/s/1VIEGrttVvxI-OsCSVZqt9A) 密码：hf2u

# 领域设计

- 领域驱动设计DDD [百度云下载链接](https://pan.baidu.com/s/1G-cJL4T418I_IEkzK4a1-Q) 密码：qxzc

# 免责声明

书籍全部来源于网络整理，我这里只是收集整理了他们的链接，如有侵权，马上联系我，我立马删除对应链接。我的邮箱：478231309@qq.com


```text
你多学一样本事，就少说一句求人的话，现在的努力，是为了以后的不求别人，实力是最强的底气。记住，活着不是靠泪水博得同情，而是靠汗水赢得掌声。
```

# [猿码天地-Java知识学堂脑图](https://www.processon.com/view/link/6035ed1f079129248a64a6af) （查看文件密码：请关注公众号【猿码天地】，回复关键字‘活到老学到老’获取）
# [猿码天地-Java超神之路脑图](https://www.processon.com/view/link/6035f068e0b34d124437e0e1) （查看文件密码：请关注公众号【猿码天地】，回复关键字‘活到老学到老’获取）
